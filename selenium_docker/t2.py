"""
A simple selenium test example written by python
"""

import unittest
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC 
import time
import random
class TestTemplate(unittest.TestCase):
    """Include test cases on a given url"""

    def setUp(self):
        """Start web driver"""
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(chrome_options=chrome_options)
        self.driver.implicitly_wait(10)

    def tearDown(self):
        """Stop web driver"""
        self.driver.quit()

    def draw(self,shape,offsets_source,offsets_target):
        ActionChains(self.driver).reset_actions()
        #click on shape in menu
        shapes_dict={'circle':'circle','rectangle':'rect'}
        # self.driver.find_element_by_xpath(f'//*[@id="region_shape_{shapes_dict.get(shape)}"]').click() 
        but=self.driver.find_element_by_xpath(f'//*[@id="region_shape_{shapes_dict.get(shape)}"]')
        #time.sleep(1)                      
        #locate image
        canvas=self.driver.find_element_by_xpath('//*[@id="canvas_panel"]')
        #print(canvas.size)
        offsets_source=(int((1/200)*offsets_source[0]*canvas.size['width']), int((1/200)*offsets_source[1]*canvas.size['height']))
        offsets_target=(int((1/200)*offsets_target[0]*canvas.size['width']),int((1/200)*offsets_target[1]*canvas.size['height']  ) )
        #move while holding
        ActionChains(self.driver).move_to_element(but).click(but).move_to_element(canvas).move_by_offset(offsets_source[0],offsets_source[1]).click_and_hold().move_by_offset(offsets_target[0],offsets_target[1]).release().perform()
        #ActionChains(self.driver).click_and_hold(canvas).move_by_offset(offsets[0],offsets[1]).release().perform()
        #ActionChains(self.driver).move_by_offset()random.randint(0,5),random.randint(0,5)
        #ActionChains(self.driver).move_to_element_with_offset(canvas,0,0).click_and_hold().move_by_offset(offsets[0],offsets[1]).release().perform()
        #ActionChains(self.driver).drag_and_drop_by_offset(canvas,1,1).perform()        
        #time.sleep(1) 
 

    def test_case_1(self):
        """
        load image in via
        draw
        screenshot
        compare
        
        """
        try:
            #go to site
            #url='https://www.robots.ox.ac.uk/~vgg/software/via/via-1.0.6.html'
            url2='https://www.robots.ox.ac.uk/~vgg/software/via/via-1.0.6.html'
            self.driver.get(url2)
            #el=self.driver.find_element_by_xpath('//*[@id="via_start_info_panel"]/p/a[1]')
            #self.driver.find_element_by_css_selector("#rso > div:nth-child(1) > div > div > div > div.r > a > h3").click()
            el=self.driver.find_element_by_xpath('//*[@id="ui_top_panel"]/div[1]/ul/li[2]/a')
            el2=self.driver.find_element_by_xpath('//*[@id="ui_top_panel"]/div[1]/ul/li[2]/div/a[1]')
            # window_before = self.driver.window_handles[0]
            ActionChains(self.driver).move_to_element(el).move_to_element(el2).click(el2).perform()
            file_add='/wrks/lens-833059_640_dark.jpg'
            #load image            
            import time
            #time.sleep(2)        
            # window_after = self.driver.window_handles[1]
            # self.driver.switch_to_window(window_after)   
            #self.driver.find_element_by_xpath("//input[@id='selectKeys']").send_keys(file_add)
            elem = self.driver.find_element_by_id("invisible_file_input")
            elem.send_keys(file_add)            
            #WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//input[@type="file"][@name="qqfile"]'))).send_keys(file_add)
            #self.driver.find_element_by_xpath('//html/body/input').send_keys(file_add)
            #ActionChains(self.driver).send_keys(file_add).send_keys(' \n').perform()

            # ActionChains(self.driver).reset_actions()
            #click on shape in menu
            shapes_dict={'circle':'circle','rectangle':'rect','polygon':'polyline'}
            shape='polygon'

            self.driver.find_element_by_xpath(f'//*[@id="region_shape_polyline"]').click()
                    
            #locate image
            canvas=self.driver.find_element_by_xpath('//*[@id="canvas_panel"]')

            #move while holding
            actions = ActionChains(self.driver)
            #actions.move_to_element(but)
            #actions.click(but)

            actions.move_to_element(canvas)
            actions.click()
            actions.move_by_offset(20,0)
            actions.click()
            actions.move_by_offset(0,20)
            actions.click()       
            actions.move_by_offset(-20,0)
            actions.click()      
            actions.move_by_offset(0,-20)
            actions.click()    
            actions.move_by_offset(0,200)
            actions.click()  
            actions.pause(1)
            # actions.move_by_offset(200,0)
            # actions.click_and_hold()
            # actions.move_by_offset(0,-5)
            # actions.release()

            # #actions.move_to_element(canvas)
            # actions.move_by_offset(-400,0)
            # actions.click_and_hold()
            # actions.move_by_offset(0,-5)
            # actions.release()            

            #.click()
            #move while holding
            #actions = ActionChains(self.driver)
            #actions.move_to_element(but)
            #actions.click(but)
            actions.perform()
            self.driver.save_screenshot('wrks/selenium_docker/aaa1.png')    
            actions.click(self.driver.find_element_by_xpath(f'//*[@id="region_shape_circle"]'))
          
            actions.move_to_element(canvas)
            actions.move_by_offset(0,100)
          
            #actions.click()
            actions.click_and_hold()
            actions.move_by_offset(0,20)
            actions.release()       
            actions.perform()      

            #self.draw('circle',(-80,0),(0,-5))
            #self.draw('rectangle',(80,0),(5,5))
            #self.driver.find_element_by_xpath('//*[@id="canvas_panel"]').screenshot('wrks/selenium_docker/aaa1.png')
            #time.sleep(1)
            #self.draw('circle',(80,0),(0,50))
            #time.sleep(1)
            # self.draw('rectangle',(-40,0),(0,-5))
            # time.sleep(1)          
            # self.draw('circle',(40,0),(0,5))
            # time.sleep(1)                                
            #self.draw('rectangle',(30,80))
            # self.driver.find_element_by_xpath('//*[@id="region_shape_circle"]').click()            
            # time.sleep(2)                      
            # #draw circle
            # canvas=self.driver.find_element_by_xpath('//*[@id="canvas_panel"]')
            # ActionChains(self.driver).drag_and_drop_by_offset(canvas,40,40).perform()
            
            #time.sleep(2) 
            self.driver.find_element_by_xpath('//*[@id="canvas_panel"]').screenshot('wrks/selenium_docker/aaa.png')
            # acts=ActionChains(self.driver)
            # acts.click(el).perform() 
            # self.driver.implicitly_wait(20)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)


    # def test_case_1(self):
    #     """Find and click Learn more button"""
    #     try:
    #         #go to site
    #         #url='https://www.robots.ox.ac.uk/~vgg/software/via/via-1.0.6.html'
    #         url2='https://seleniumhq.github.io/selenium/docs/api/py/webdriver/selenium.webdriver.common.action_chains.html'
    #         self.driver.get(url2)
    #         #el=self.driver.find_element_by_xpath('//*[@id="via_start_info_panel"]/p/a[1]')
    #         #self.driver.find_element_by_css_selector("#rso > div:nth-child(1) > div > div > div > div.r > a > h3").click()
    #         self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/ul/li/a').click()
    #         # window_before = self.driver.window_handles[0]
    #         # ActionChains(self.driver).move_to_element(el).context_click(el).perform()

    #         #load image            
    #         import time
    #         time.sleep(2)        
    #         # window_after = self.driver.window_handles[1]
    #         # self.driver.switch_to_window(window_after)                 
    #         self.driver.save_screenshot('wrks/selenium_docker/aaa.png')
    #         # acts=ActionChains(self.driver)
    #         # acts.click(el).perform() 
    #         # self.driver.implicitly_wait(20)
    #         self.driver.close()
    #     except NoSuchElementException as ex:
    #         self.fail(ex.msg)


    # def test_case_1(self):
    #     """Find and click Learn more button"""
    #     try:
    #         #go to site
    #         self.driver.get('https://www.robots.ox.ac.uk/~vgg/software/via/via-1.0.6.html')
    #         #load image            
    #         el=self.driver.find_element_by_xpath('//*[@id="via_start_info_panel"]/p/a[1]')
    #         ActionChains(self.driver).move_to_element(el).click_and_hold(el).perform()
    #         import time
    #         time.sleep(2)             
    #         el.save_screenshot('wrks/selenium_docker/aaa.png')
    #         # acts=ActionChains(self.driver)
    #         # acts.click(el).perform() 
    #         # self.driver.implicitly_wait(20)
    #     except NoSuchElementException as ex:
    #         self.fail(ex.msg)



    # def test_case_1(self):
    #     """Find and click Learn more button"""
    #     try:

    #         self.driver.get('http://svbrubio:5015')
    #         el = self.driver.find_element_by_xpath('/html/body/maintag/navbar/nav/h5/a/img')    
    #         tmp=el.get_attribute("src")      
    #         print(tmp)  
    #         assert tmp=='http://svbrubio:5015/hazera_log_icon_small_white2.png'
    #     except NoSuchElementException as ex:
    #         self.fail(ex.msg)

    # def test_case_2(self):
    #     """Find and click Learn more button"""
    #     try:

    #         self.driver.get('http://localhost:3000')
    #         el = self.driver.find_element_by_xpath('//*[@id="routing_point"]/div').value_of_css_property("background")
    #         assert el=='rgb(0, 0, 0) url("http://localhost:4021/img/lens-833059_640_dark.jpg") no-repeat scroll 0% 0% / 100% padding-box border-box'
    #     except NoSuchElementException as ex:
    #         self.fail(ex.msg)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestTemplate)
    unittest.TextTestRunner(verbosity=2).run(suite)

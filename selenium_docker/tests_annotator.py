"""
A simple selenium test example written by python
"""

import unittest
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC 
import time
import random
class TestTemplate(unittest.TestCase):
    """Include test cases on a given url"""

    def setUp(self):
        """Start web driver"""
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(chrome_options=chrome_options)
        self.driver.implicitly_wait(10)

    def tearDown(self):
        """Stop web driver"""
        self.driver.quit()

    def login(self):
        """
        log in
        load first image with annotations
        """
        try:
            #go to site
            driver = self.driver
            driver.get("https://annotator-dev.agri-net.org.il/#")
            try:
                driver.find_element_by_link_text("logout amits").click()               
            except NoSuchElementException as ex:
                pass
            driver.find_element_by_id("username").clear()
            driver.find_element_by_id("username").send_keys("amits")
            driver.find_element_by_id("password").clear()
            driver.find_element_by_id("password").send_keys("2wsx2wsx")   
            driver.find_element_by_xpath('/html/body/maintag/navbar/nav/h4/loginbox/div[1]/div[3]/a').click() 

        except NoSuchElementException as ex:
            self.fail(ex.msg) 

    # def test_case_1(self):
    #     """
    #     log in
    #     load first image with annotations
    #     """
    #     try:
    #         self.login()
    #         driver = self.driver
    #         driver.find_element_by_link_text("test1").click()
    #         time.sleep(40)

            
    #         #locate image  
    #         canvas=driver.find_element_by_id("main_img_canvas")    
    #         canvas.screenshot('wrks/selenium_docker/test1_first_image_test.png')
    #         assert open("wrks/selenium_docker/test1_first_image_test.png","rb").read() == open("wrks/selenium_docker/test1_first_image.png","rb").read()

    #         #draw new cirlce
    #         #find circle button
    #         driver.find_element_by_xpath('//*[@id="object_drawing_circle_div"]/span').click()  

    #         #locate image  
    #         canvas=driver.find_element_by_id("main_img_canvas")    


    #         #action sequence
    #         actions = ActionChains(self.driver)

    #         actions.move_to_element(canvas)
    #         actions.move_by_offset(100,-80) 
    #         actions.click_and_hold()
    #         actions.move_by_offset(0,20)   
    #         actions.release()
    #         actions.perform()     
    #         canvas.screenshot('wrks/selenium_docker/test1_new_circ_test.png')
    #         assert open("wrks/selenium_docker/test1_new_circ.png","rb").read() == open("wrks/selenium_docker/test1_new_circ_test.png","rb").read()
    #         #delete circle
    #         driver.find_element_by_xpath('//*[@id="edit_box"]/div[1]/i').click() 

    #         canvas.screenshot('wrks/selenium_docker/test1_first_image_after_delete_test.png')
    #         assert open("wrks/selenium_docker/test1_first_image_after_delete.png","rb").read() == open("wrks/selenium_docker/test1_first_image_after_delete_test.png","rb").read()            
    #     except NoSuchElementException as ex:
    #         self.fail(ex.msg)   

    def test_case_2(self):
        """
        load new image, annotate, check, delete ,check again
        """
        try:
            #go to site
            self.login()            
            driver = self.driver
            driver.find_element_by_link_text("test1").click()
            element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, '4493')))

            driver.find_element_by_id("4493").click()
            time.sleep(10)
            driver.find_element_by_id("4493").click()
            time.sleep(40)
            #element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="object_drawing_circle_div"]/span')))
              



            #draw circle
            #find circle button
            driver.find_element_by_xpath('//*[@id="object_drawing_circle_div"]/span').click()  

            #locate image  
            canvas=driver.find_element_by_id("main_img_canvas")    


            #action sequence
            actions = ActionChains(self.driver)

            actions.move_to_element(canvas)
            actions.move_by_offset(50,0) 
            actions.click_and_hold()
            actions.move_by_offset(0,20)   
            actions.release()
            actions.perform()


            #draw rectangle
            #find rectangle button
            driver.find_element_by_xpath('//*[@id="object_drawing_rectangle_div"]/span').click()  

            #locate image  
            canvas=driver.find_element_by_id("main_img_canvas")    


            #action sequence
            actions = ActionChains(self.driver)

            actions.move_to_element(canvas)
            actions.move_by_offset(-50,0) 
            actions.click_and_hold()
            actions.move_by_offset(20,-20)   
            actions.release()
            actions.perform()  


            #draw ploygon
            #find rectangle button
            driver.find_element_by_xpath('//*[@id="polygons_draw_div"]/span').click()  

            #locate image  
            canvas=driver.find_element_by_id("main_img_canvas")    


            #action sequence
            actions = ActionChains(self.driver)

            actions.move_to_element(canvas)
            actions.move_by_offset(0,-80) 
            actions.click_and_hold()
            for i in range(8):
                actions.move_by_offset(10,10)
                time.sleep(1)           
            actions.move_by_offset(-50,-50)
            time.sleep(1)  
            actions.release()
            actions.move_to_element(canvas)
            actions.move_by_offset(0,-80)             
            actions.click()
            actions.perform()             
            time.sleep(10)            
            canvas.screenshot('wrks/test2.png')
            assert open("wrks/test2_test.png","rb").read() == open("wrks/test2.png","rb").read()
        except NoSuchElementException as ex:
            self.fail(ex.msg)   


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestTemplate)
    unittest.TextTestRunner(verbosity=2).run(suite)

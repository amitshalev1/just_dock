echo "[]" >> datasets.json
echo '[{"name": "2017_01_22_cucumber_Blossom end shape", "img_source": "example/cucumbers/", "possible_values": "0,1,2,3,4,5,6,7,8,9", "trait": "Blossom end shape", "users": "Adam Call,Alain Lecompte,Harel Belotserkovsky", "img_target": "772", "type": "simple scoring", "crop": "cucumber", "object": "fruit", "dev": "mature", "treatment": "none", "anno_order": "random", "allow_repeat": "No", "user_name": "dror", "task_id": 2}]' >> tasks.json
mkdir results
python -m flask run -h 0.0.0.0 -p 5002


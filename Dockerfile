FROM debian:latest

#  $ docker build . -t continuumio/miniconda3:latest -t continuumio/miniconda3:4.5.4
#  $ docker run --rm -it continuumio/miniconda3:latest /bin/bash
#  $ docker push continuumio/miniconda3:latest
#  $ docker push continuumio/miniconda3:4.5.4

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH /opt/conda/bin:$PATH

RUN apt-get update --fix-missing && \
    apt-get install -y wget bzip2 ca-certificates curl git && \    
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*


RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-4.5.4-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    /opt/conda/bin/conda clean -tipsy && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate base" >> ~/.bashrc

ENV TINI_VERSION v0.16.1
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini
RUN chmod +x /usr/bin/tini
#RUN conda install -c conda-forge opencv 
RUN apt-get update && \
    apt install libgl1-mesa-glx -y && \
    apt update && \
    apt install -y libsm6 libxext6 && \
    apt update && \
    apt-get install -y libfontconfig1 libxrender1 && \
    apt update && \
    apt-get install -y libgtk2.0-dev
#VOLUME maagad_annotation_2018/
RUN git clone https://amitshalev1@bitbucket.org/drhilman/maagad_annotation_2018.git
COPY req.txt requirements.txt
COPY prepare_docker.sh prepare_docker.sh
RUN pip install -r requirements.txt && \
    pip install opencv-python && \
    conda install -c anaconda bcolz -y
#RUN conda install --yes --file requirements.txt
#RUN while read requirement; do conda install --yes $requirement || pip install $requirement; done #< requirements.txt
ENTRYPOINT [ "/usr/bin/tini", "--" ]

RUN export FLASK_APP=app.py && \
    export FLASK_DEBUG=0
WORKDIR maagad_annotation_2018/
#CMD [ "cd maagad_annotation_2018 && python -m flask run -h 0.0.0.0 -p 5002" ]
CMD [ "/bin/bash" ]

#start
echo "testing environment ...." >&2

## declare an array variable
declare -a arr=("docker" "git" "vim")

## now loop through the above array
for i in "${arr[@]}"
do
to_test="$i"
echo "checking if $to_test is installed"
if ! [ -x "$(command -v $to_test)" ]; then
  echo "Error: $to_test is not installed." >&2
else
  echo "installed" >&2
fi
   # or do whatever with individual element of the array
done
nvidia-smi
if [ $? -eq 0 ]; then
    gpu_or_cpu='gpu'
else
    gpu_or_cpu='cpu'
    echo "no working gpu"
fi

if [ "$gpu_or_cpu" = "gpu" ]; then 
    to_test="nvidia-docker"
    echo "checking if $to_test is installed" >&2
    if ! [ -x "$(command -v $to_test)" ]; then
    echo "Error: $to_test is not installed." >&2
    else

        nvidia-docker run -it ufoym/deepo:all-py36-jupyter python -c "import tensorflow as tf; \
            a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a'); \
            b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b'); \
            c = tf.matmul(a, b); \
            sess = tf.Session(config=tf.ConfigProto(log_device_placement=True)); \
            print(sess.run(c));"


    fi

else
        docker run -it tensorflow/tensorflow:nightly-py3-jupyter python -c "import tensorflow as tf; \
            a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a'); \
            b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b'); \
            c = tf.matmul(a, b); \
            sess = tf.Session(config=tf.ConfigProto(log_device_placement=True)); \
            print(sess.run(c));"    
fi

